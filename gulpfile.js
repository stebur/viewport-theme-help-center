// Example gulpfile.js
// install all dependencies first (run npm install)
//
// Configuration:
//   TARGET -- the target to deploy to
//
// Tasks:
//   upload -- full build & upload
//   reset-theme -- remove all files from theme
//   watch -- watch (to be used during development)

var browserSync     = require('browser-sync').create();
var gulp            = require('gulp');
var ViewportTheme   = require('gulp-viewport');
var $               = require('gulp-load-plugins')();
var concat          = require('gulp-concat');
var minify         = require('gulp-minify');

// The target system needs to match with a section in .viewportrc
// How to use the different environments within the .viewportrc file is explained here: https://github.com/K15t/gulp-viewport#get-started
var TARGET = 'DEV';

// !! Create Theme in Viewport !!
// Before you can upload your theme there must be a Viewport theme with the exact same name like this THEME_NAME
var THEME_NAME = 'Simple Help-Center Theme 1';

// The url to your viewport, if you use browsersync.
// A tool to automatically refresh the browser when watching files. See https://www.browsersync.io/
var BROWSERSYNC_URL = 'http://localhost:1990/';



var viewportTheme = new ViewportTheme({
    env: TARGET,
    themeName: THEME_NAME,
    sourceBase: 'src'
});



// Creates the Viewport Theme with name found in THEME_NAME in Confluence
gulp.task('create', function() {
    if(!viewportTheme.exists()) {
        viewportTheme.create();
    } else {
        console.log('Theme with name \'' + THEME_NAME + '\' already exists.');
    }
});



// Uploads all files
gulp.task('upload', ['reset-theme', 'fonts', 'img', 'js', 'sass', 'templates', 'override']);



// Uploads all files, watch changes on files and reloads automatically browser window
gulp.task('watch',['upload'], function () {
    browserSync.init({
        proxy: BROWSERSYNC_URL
    });

    viewportTheme.on('uploaded', browserSync.reload);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('src/img/**/*', ['img']);
    gulp.watch('src/js/**/*', ['js']);
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/**/*.vm', ['templates']);
});



gulp.task('fonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build/fonts'));
});



gulp.task('img', function () {
    return gulp.src('src/img/*')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build/img'));
});



gulp.task('js', function () {
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/what-input/dist/what-input.js',
        './bower_components/foundation-sites/dist/js/foundation.js',
        './node_modules/lightbox2/src/js/lightbox.js',
        //Insert additional external .js files here
        'src/js/**/*.*'])
        .pipe(concat('build/js/app.js'))
        .pipe(minify())
        .pipe(gulp.dest(''))
        .pipe(viewportTheme.upload({
            sourceBase: 'build/js/app-min.js',
            targetPath: 'js/app.js'
        }))
});



// Paths for sass-files of ZURB Foundation
var sassPaths = [
    './bower_components/normalize.scss/sass',
    './bower_components/foundation-sites/scss',
    './bower_components/motion-ui/src'
];

// sass compile & concat into one file
gulp.task('sass', function() {
    return gulp.src('src/scss/app.scss')
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' // if css compressed **file size**
        })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('build/css'))
        .pipe(viewportTheme.upload(
            {
                sourceBase: 'build/css/app.css',
                targetPath: 'css/app.css'
            }
        ))
});



gulp.task('templates', function () {
    return gulp.src('src/**/*.vm')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build'));
});


gulp.task('override', function () {
    return gulp.src('src/overrides/*.vm')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build'));
});



// Reset your theme (empties the Theme directory in Confluence)
gulp.task('reset-theme', function () {
    viewportTheme.removeAllResources();
});

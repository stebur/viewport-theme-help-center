$(document).foundation();

$(function() {
    // Insert your custom functions here
    // Don't forget to include your external files in your gulp-file 'js' task

    if ($('.js-toc').length > 0) {


        $.when(removeSpecialCharFromID(),tocbot.init({
            // Where to render the table of contents.
            tocSelector: '.js-toc',
            // Where to grab the headings to build the table of contents.
            contentSelector: '.article__content',
            // Which headings to grab inside of the contentSelector element.
            headingSelector: 'h2, h3, h4',

            scrollSmooth: true,
            scrollSmoothDuration: 100,

            throttleTimeout: 50
        }));

        function removeSpecialCharFromID() {
            $.each($('h2, h3, h4'), function(index) {
                $(this).attr('id', index);
            });
        }
    }

    // Search
    $('#search-input').on('keyup', function () {
        if ($('#search-input').val().length >= 4) {
            $('#search-results').load($('#search').attr('action'), "quicksearch=true&max=5&q=" + $('#search-input').val());
            $('.quicksearch').addClass('quicksearch--results');

            setTimeout(function () {
                if ($('#search-results ul').has("li").length < 1) {
                    $('.quicksearch').html("<li class='quicksearch--result'><p>No results found</p></li>");
                }
            }, 100)
        }
        else if ($('#search-input').val().length <= 3) {
            $('.quicksearch').removeClass('quicksearch--results');
        }
    });

    // Navigation
    if ($('#navigationMenu').length > 0) {
        var navigation = $('#navigationMenu');
        var picker = $('#picker');
        var id;
        resizeNavigation();

        $(window).resize(function() {
            clearTimeout(id);
            id = setTimeout(resizeNavigation, 500);
        });

        function resizeNavigation() {
            var viewport = $(window).width();
            if(viewport <= 1024){
                navigation
                    .removeClass('large-2 sticky-container')
                    .addClass('off-canvas-absolute position-left');
                navigation.attr('data-transition', 'overlap');
                // Version-Variant Picker
                picker.addClass('text-right');
                picker.css('order', 3);

            } else if (viewport > 1024) {
                navigation
                    .removeClass('off-canvas-absolute position-left')
                    .addClass('large-2 sticky-container');
                navigation.removeAttr('data-transition', 'overlap');
                // Version-Variant Picker
                picker.removeClass('text-right');
                picker.css('order', 2);
            }
        }


        $('#activePage').parents('ul')
            .removeClass('invisible')
            .addClass('is-active');
    }

    // article content manipulation
    if ($('#articleContent').length > 0) {

        // Image reveal
        $('.article__content img').each(function() {
            $(this).attr('data-open', 'imageContainer');
            var fileName = /[^/]*$/.exec($('.article__wrapper img').attr('src'))[0];
            var imageName = fileName.split('.').slice(0, -1).join('.');
            $(this).attr('title', imageName);
            $(this).attr('alt', imageName);
        });

        $('#articleContent img').on('click', function(e) {
            e.preventDefault();
            var containerImage = $('#containerImage');
            var imageSource = $(this).attr('src');
            var imageName = $(this).attr('title');
            containerImage.attr({"src": imageSource, "title": imageName, "alt": imageName});
        });


        // Table scrollable
        $('.table-wrap').addClass('table-scroll');

    }

    // Replace standard colors
    if($('meta[name="colorPrimary"]').attr('content').length > 0 || $('meta[name="colorSecondary"]').attr('content').length > 0) {
        var customColorPrimary = $('meta[name="colorPrimary"]').attr('content');
        var customColorSecondary = $('meta[name="colorSecondary"]').attr('content');

        $.when(colorReplace('#005D7A', customColorPrimary), colorReplace('#fefefe', customColorSecondary)).then(function() {
            $('#loaderOverlay').remove();
        });

        function colorReplace(findHexColor, replaceWith) {

            var keys = Object.keys($('html').get(0).style);
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(findHexColor);
            var rgb = 'rgb(' + parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) + ')';
            for (var k in keys) {
                key = keys[k].replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
                if (key.indexOf('color') > -1) {
                    $("*").each(function () {
                        if ($(this).css(key) === rgb) {
                            $(this).css(key, replaceWith);
                        }
                    });
                }
            }

        }
    }
});
